"""
@date: 2/26/2020
@author: Pau Lleonart Calvo
@PID: paullc
@assignment: Learning recursion.
@note: Do NOT alter the function headers that are well documented
"""

# This implementation is quite simple. If we do the modulo 10 of
# the number we get the last digit, then we recursively call sum_digits
# on number without the last digit by integer dividing the number by 10. Once
# the last division occurs, the number will be 0, so that is the base case. 
def sum_digits(number: int) -> int:
    """ Sums each digit of a number together using recursion
    @param number: an integer whose digits will be summed
    @return: the sum of all digits in the number
    """
    if number == 0:
        return 0
    return (abs(number) % 10) + sum_digits(abs(number) // 10)

# This function calls the helper funciton diff_two_helper() in order to provide necessary
# recursive parameters. 
def is_diff_two(values: list, diff: int) -> bool:
    """ Checks if there are two elements within a list that have a specific difference between them using recursion 
    @param values: The list of integer values to be searched
    @param diff: The difference value between two elements to find
    @return: True if there are two elements in values with a difference of diff, otherwise False
    """

    return diff_two_helper({}, 0, values, diff)


# This helper function utilizes a map in order to access elements in O(1) time. 
# If an element does not have its complement relative to diff in the map, then the element
# is added to the map and that map is passed to the next recursive call. If any value
# has a valid complement in the list, it will be found within the map by the time
# the recursive call index parameter reaches the end of the list. The base cases
# are that the end of the list is reached without finding a match, or 
# a match is found and True is returned. 
def diff_two_helper(dic: dict, index: int, values: list, diff: int) -> bool:
    if (index >= len(values)):
        return False

    if ((values[index] + diff in dic) or (values[index] - diff in dic)):
        return True
    else:
        dic[values[index]] = 1

    
    return diff_two_helper(dic, index + 1, values, diff)
