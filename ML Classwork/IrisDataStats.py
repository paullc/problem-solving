'''
https://machinelearningmastery.com/machine-learning-in-python-step-by-step/
Loads a dataset with assosciated attribute names, then reports on details
of the dataset including statistics and graphs
'''

# Load libraries
import pandas
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt


# Load dataset
# File represents the path to the needed .csv file.
file = "G:\My Drive\Classes\Spring 2020\Problem Solving in CS\Programs\ML Classwork\iris.csv"

# Names identifies the data categories needed for pandas to properly label the values.
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']

# Pandas reads the csv file and creates a Pandas dataframe out of it
dataset = pandas.read_csv(file, names=names)

# Prints the "shape" or dimensions of the dataset, meaning the total rows and columns. 
print("Data has the following number of rows and colums. (Rows, Columns)\n")
print(dataset.shape)
print("\n")

# Prints the first 20 rows of the csv file so that some of the data can be seen. 
print("The first 20 rows of data are the following:\n ")
print(dataset.head(20))
print("\n")

# Shows different relevant measurements of the data, like the mean valule and max value of each 
# category of data.
print("Some relevant statistics about this dataset:\n ")
print(dataset.describe())
print("\n")

# shows how many measurements are in each "class" of data and shows the type of the data.
print("The data is grouped into the following classes:\n ")
print(dataset.groupby('class').size())
print("\n")

# Plots box and whisker graphs for each data category.
dataset.plot(kind='box', subplots=True, layout=(2,2), sharex=False, sharey=False)
plt.show()

# Plots histograms for each data category.
dataset.hist()
plt.show()

# Plots a scatter matrix for each data category
scatter_matrix(dataset)
plt.show()

