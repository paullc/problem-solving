'''
HW 5 Template

@author: Pau Lleonart Calvo 
@pid: paullc
@CRN: 12936
@date: 2/21/2020
@Honor Code Pledge: I have neither given nor received unauthorized assistance on this assignment
'''

# NOTES: The big O complexity of this algorithm is O(N) as the list
# is only traversed once.
def max_difference(values: list) -> float:
    """ Efficiently finds the largest difference between any two elements in a list
    @param values: a list of numbers
    @return number for the largest difference between elements

    """
    smallest, largest = values[0], values[0]
    for number in values:
        if number < smallest:
            smallest = number
        if number > largest:
            largest = number
    return largest - smallest

# NOTES: The big O complexity of this algorithm is O(N) as the list
# is only traversed once in total since the indeces of the right 
# and left side converge at an arbitrary point in the list, but never
# move past eachother.
def sort_bivalued(values: list) -> list:
    """Efficiently sort a list of binary values
    @param values: a list of binary digits (0 or 1)
    @return: a list of binary numbers in ascending sort order
    """
    rightPos = -1
    for i in range(len(values) - 1):
        if (i - rightPos) >= len(values):
            break

        if values[i] == 1:
            
            while (values[rightPos] != 0):
                rightPos -= 1
                if (abs(rightPos) >= len(values)):
                    return values

            values[i], values[rightPos] = 0, 1
            
            rightPos -= 1
    return values 
        

