#!/usr/bin/python
import sqlite3
import sys

conn = sqlite3.connect('cs_course_scheduling.db')
print("Opened database successfully")

cursor = conn.execute(
    f"SELECT first_name, last_name, academic_year FROM students WHERE academic_year={sys.argv[1]} ORDER BY last_name")
for row in cursor:
   print("First Name    = ", row[0])
   print("Last Name     = ", row[1])
   print("Academic Year = ", row[2])
   print("----------------------------------------")

print("Operation done successfully")
conn.close()
