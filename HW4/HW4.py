'''
HW 4 Template

@author: Pau Lleonart Calvo
@pid: paullc    
@CRN: 12936
@date: 02/14/2020
@Honor Code Pledge: I have neither given nor received unauthorized assistance on this assignment.
'''

'''This algorithm has a time complexity of O(N) as the time to return depends
directly on the number of iterations of the while loop.

i.      Set the exponent variable to 1;
        Then, while 2 ** exponent is less than the number, increment the exponent variable.
        Then, if 2 ** exponent is exactly the number, return the exponent
        Else, return between exponent - 1 and exponent. 


ii.     The log base 2 of a number is, in plain terms, the number to which the base 
        must be raised to produce the parameter. So, here we can take the base and raise
        it to the power of the exponent variable and check whether it is still less than the number.
        If it is stil less, then we increment the exponent and go through the loop again until it is
        greater than or equal to the number. Then, if the number is equal to 2 ** exponent, we know 
        the result is the exact integer value of exponent. Else, we know that the value lies somewhere
        between this iteration of the loop and the last, so the value must be between exponent - 1 and 
        exponent. 

iii.    Case 1: Negative Number: Negative numbers are out of the range of a log function, so pass
        Case 2: Zero: Zero is out of the range of a log function, so pass
        Case 3: Number == 1: Exponent starts at 0, so properly returns 0
'''
def log_base_2(number: float) -> str:

    if(number <= 0):
        return "Invalid Input"
    
    exponent = 0;

    if number > 1:
        while (2 ** exponent) < number:
            exponent += 1
    else:
        while (2 ** exponent) > number:
            exponent -= 1

    if (2 ** exponent) == number:
        return str(exponent)
    else:
        return "between " + str(exponent - 1) + " and " + str(exponent) if exponent > 0 else \
            "between " + str(exponent + 1) + " and " + str(exponent)

print(log_base_2(0))
print(log_base_2(1))
print(log_base_2(256))
print(log_base_2(81))
print(log_base_2(0.5))

'''This algorithm also has a time complexity of O(N) as the time to return depends
directly on the number of elements in the list

i.      For i in range of half the length of the list
        Store the left element in a temporary variable
        Set the left side element to be the opposite side's value
        Set the right side element to be the stored temp variable
        Increment i by 2
        Return the altered my_list.


ii.     For this algorithm, you just want to make sure you iterate over only half of the string at maximum
        so that you don't double switch any elements. Hence, you want to make sure that for an even number of
        elements you go to len(list) / 2, but for an odd number of elements you want to go to len(list) // 2.
        So, we use // to make sure we always get an integer for the final index. This is because for an odd 
        number of elements, the center element does not have to be swapped. For each element we check from the
        left, we temporarily store its value so as to not lose the data, then put the right side value in the
        left side element. Then we set the right element to be the temp value. For each iteration of the loop
        we increment i by 2 to produce the desired alternating element effect.
        Then after the loop finishes we have an altered my_list and we return it. 

iii.    Case 1: Empty List: If its empty there is nothing to reverse, so it returns an empty list.
        Case 2: One Element: For loop functions properly and does no work so it returns the original list.
        Case 3: Negative Numbers: No difference, there is no math to be done with the elements themselves
                so there is no difference.
        Case 4: Even Number of Elements: Swaps elements up to len(my_list) / 2. Swaps exactly half of the elements.
        Case 5: Odd Number of Elements: Swaps elements up to len(my_list) // 2. Swaps up to, but not including, the
                middle element. 
'''
def rearrange_list(my_list: list) -> list:
    for i in range(0,len(my_list) // 2, 2):
        temp = my_list[i]
        my_list[i] = my_list[(i + 1) * -1]
        my_list[(i + 1) * -1] = temp

    return my_list


print(rearrange_list([1, 2, 3, 4, 5, 6, 7, 8]))
print(rearrange_list([1, 2, 3, 4, 5, 6, 7, 8, 9]))
print(rearrange_list([1, 2]))
print(rearrange_list([1, 2, 3]))
