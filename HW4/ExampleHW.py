'''
Created on Feb 9, 2019

@author: techstaff
'''
"""
@date: 2/9/19
@author: Margaret Ellis
@PID: maellis1
@assignment: to show students how to format HW4 and explain their probelmsovling
@note: Do NOT alter the function headers that are well documented, 
Do put your hw answers in the spaces provided within function headers
"""


def print_factors(number: int):
    """  Print all the factors and if the given number is prime, then it should print that it is prime
    @param number: float to calculate on
    @precondition: number is > 0
    @return str: A string description of the approximate result
    parts i-iii from HW
    i. Work out the steps to figure out the 2 concrete examples 
     step by step and briefly explain your work and thinking(5pts)
         Given 10
        Start at 1 and check all the integers up to 5
        10 is divisible by 1, so 1 and 10
        10 is divisible by 2 , so 2 and 5
        10 is not divisible by 3
        10 is not divisible 4 and then can stop checking because 5 is 10/2
        Given 7
        10 is divisible by 1, so 1 and 7
        7 is not divisible by 2
        7 is not divisible by 3 then can stop checking because 4 is 7//2
        So it’s prime because no factors starting at 2

    ii. Find and describe a pattern and attempt to generalize (5pts)
        Print 1 and the given number because they will always be factors. Then start at 2
        and check if the number is divisible by the divisor using %. If the remainder is 0
        then print the number. Can do this with a loop to the given number or could also
        only loop half way but then need to print the number you are checking and the
        results when dividing it into the given number..

    iii. Investigate and explain special cases to see if the pattern holds up (5pts)
        0 is not positive, and by definition 1 is not prime either
    """
    if number == 1:
        print(1)
    else:
        prime = True
        print("\n%d,%d" % (1,number),end="")
        for i in range(2,number//2):
            if (number % i == 0):               
                prime = False
                print(",%d,%d" % (i,number/i),end="")
            if prime:
                print(" so",number, "is prime" ) 

#print_factors(10)
#print_factors(7)
#print_factors(1)