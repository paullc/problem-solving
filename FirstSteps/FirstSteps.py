'''
@author: paullc
@version: 2/9/2020

'''


def is_valid_ccn(cn: str) -> bool:
    if len(cn) != 16:
        return False

    cardSum = 0
    secondSpot = False
    for digitPos in range(len(cn) - 1, -1, -1):
        number = int(cn[digitPos])

        print("Position: " + str(digitPos))
        if (secondSpot):
            number *= 2
            print("Doubled Number: Now " + str(number))
            if (number > 9):
                numTooLarge = str(number)
                number = int(numTooLarge[0]) + int(numTooLarge[1])
                print("Number too large: Now " + str(number))
        cardSum += number
        secondSpot = not secondSpot

    print(cardSum)
    return cardSum % 10 == 0


print(is_valid_ccn("4716296157521221"))


def is_a_leap_year(year: int) -> bool:

    if (year % 4 == 0):
        if (year % 100 == 0):
            if (year % 400 == 0):
                return True
            return False
        return True
    return False


def calculate_product(numbers: list) -> int:
    product = 1
    for number in numbers:
        product *= number
    return product
